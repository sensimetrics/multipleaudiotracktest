package com.sens.multipleaudiotrack;


import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by michel on 3/1/16.
 */

public class MultipleAudioTrack {
    private static final String tag = "MultipleAudioTrack";

    private final Context context;
    private final String[] filenames;


    private final int audioSampleRate = 22500;
    private final int audioBufferSizeBytes = AudioTrack.getMinBufferSize(audioSampleRate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
    private final int audioBufferSizeShorts = audioBufferSizeBytes * Byte.SIZE / Short.SIZE;
    private Thread[] audioThreads = null;
    private AudioTrackWrapper[] audioTrackWrappers = null;
    private AudioTrack[] audioTracks = null;

    private static final int gainChangingBuffers = 1;

    private volatile boolean startPlaying = false;
    private volatile boolean stopPlaying = false;

    private volatile boolean gainChanging = false;
    private volatile int gainChangingTickCount = 0;
    private volatile float[] gains;

    public MultipleAudioTrack(Context context, String[] filenames) {
        if (filenames.length < 2) {
            throw new IllegalArgumentException("Must provide at least two filenames");
        } else if (filenames.length > 4) {
            throw new IllegalArgumentException("Not expected to function with more than 4 audio files");
        }

        for (String filename : filenames) {
            if ((filename == null) ||
                    filename.isEmpty() ||
                    (filename.length() == 0) ||
                    (filename.trim().length() == 0)) {
                throw new IllegalArgumentException("Filename cannot be empty, null, or all whitespace");
            }
        }

        this.filenames = new String[filenames.length];
        System.arraycopy(filenames, 0, this.filenames, 0, filenames.length);

        this.gains = new float[filenames.length];
        this.gains[0] = 1.0f;
        for (int i = 1; i < filenames.length; i += 1) {
            this.gains[i] = 0.0f;
        }

        audioThreads = new Thread[filenames.length];
        audioTrackWrappers = new AudioTrackWrapper[filenames.length];
        audioTracks = new AudioTrack[filenames.length];

        this.context = context;

//        Log.d(tag, "audioBufferSizeBytes: " + audioBufferSizeBytes);
//        Log.d(tag, "audioBufferSizeShorts: " + audioBufferSizeShorts);
//        Log.d(tag, "filenames.length: " + filenames.length);
    }

    public void startPlaying() throws IOException {
        startPlaying = true;

        for (int i = 0; i < filenames.length; i += 1) {
            audioTrackWrappers[i] = new AudioTrackWrapper(i);
        }

        for (int i = 0; i < filenames.length; i += 1) {
            audioThreads[i] = new Thread(audioTrackWrappers[i]);
        }

        for (int i = 0; i < filenames.length; i += 1) {
            audioThreads[i].start();
        }
    }

    public void stopPlaying() throws InterruptedException {
        stopPlaying = true;

        for (int i = 0; i < filenames.length; i += 1) {
            audioThreads[i].join();
        }
    }

    public boolean isPlaying() {
        return startPlaying && !stopPlaying;
    }

    public void reset() throws InterruptedException {
        stopPlaying();

        startPlaying = false;
        stopPlaying = false;

    }

    public float getGain(int trackNum) {
        if ((trackNum >= 0) && (trackNum < gains.length)) {
            return gains[trackNum];
        } else {
            throw new IllegalArgumentException("trackNum may not be < 0 or > number of audio files");
        }
    }

    public void setGain(int trackNum, float newGain) {
        if ((trackNum >= 0) && (trackNum < gains.length)) {
            gains[trackNum] = newGain;
            gainChangingTickCount = 0;
            gainChanging = true;
        } else {
            throw new IllegalArgumentException("trackNum may not be < 0 or > number of audio files");
        }
    }

    public void switchTo(int trackNum) {
        if ((trackNum >= 0) && (trackNum < gains.length)) {
            for (int i = 0; i < gains.length; i += 1) {
                gains[i] = (i == trackNum) ? 1.0f : 0.0f;
            }
            gainChangingTickCount = 0;
            gainChanging = true;
        } else {
            throw new IllegalArgumentException("trackNum may not be < 0 or > number of audio files");
        }
    }


    class AudioTrackWrapper implements Runnable {
        private final int trackNum;
        private final byte[] samples;
        final DataInputStream ds;

        public AudioTrackWrapper(int trackNum) throws IOException {
            super();

            this.trackNum = trackNum;

            final int id = context.getResources().getIdentifier(filenames[trackNum], "raw", context.getPackageName());
            final InputStream is = context.getResources().openRawResource(id);
            final BufferedInputStream bs = new BufferedInputStream(is);
            ds = new DataInputStream(bs);

            // prime the audio pump
            samples = new byte[audioBufferSizeBytes];
            // skip 44-byte WAV header
            ds.skipBytes(44);
            fillBuffer(ds);

            // not the sample rate I thought these files had. don't understand
            audioTracks[trackNum] = new AudioTrack(AudioManager.STREAM_MUSIC,
                    audioSampleRate,
                    AudioFormat.CHANNEL_OUT_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    audioBufferSizeBytes,
                    AudioTrack.MODE_STREAM);
        }

        private void fillBuffer(DataInputStream ds) throws IOException {

            if (ds.available() <= 0) {
                ds.close();

                for (int i = 0; i < samples.length; i += 1) {
                    samples[i] = 0;
                }

                stopPlaying = true;
            } else {
//                Log.d(tag, "samples.length: " + samples.length);
//                Log.d(tag, "gains.length: " + gains.length);
//                Log.d(tag, "trackNum: " + trackNum);
                for (int i = 0; (ds.available() > 0) && (i < samples.length); i += 1) {
//                    Log.d(tag, "i: " + i);

                    // TODO: take into account whether or not gains have changed, smooth to new value
                    samples[i] = (byte) Math.round(gains[trackNum] * ds.readShort());
                    // TODO: if everyone has had a shot at changing gain, then reset gain flags
                }
            }
        }

        @Override
        public void run() {
            audioTracks[trackNum].play();

            while(isPlaying()) {
                int written = audioTracks[trackNum].write(samples, 0, samples.length);

                if (written > 0) {
                    try {
                        fillBuffer(ds);
                    } catch (IOException e) {
                        Log.e(tag, "error in fillBuffer", e);
                        e.printStackTrace();
                    }
                }
            }

            audioTracks[trackNum].stop();
        }
    }

}
