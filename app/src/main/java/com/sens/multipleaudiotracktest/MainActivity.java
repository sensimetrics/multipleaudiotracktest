package com.sens.multipleaudiotracktest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sens.multipleaudiotrack.MultipleAudioTrack;

import java.io.IOException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private static final String tag = "MainActivity";

    //    private static final String[] filenames = { "anml_7084_stereo_0db", "anml_7084_stereo_20db", "anml_7084_stereo_40db" };
    private static final String[] filenames = {"airportannouncement_none_hdphns", "airportannouncement_army95_hdphns"};

    private MultipleAudioTrack mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button createMAT = (Button) findViewById(R.id.createMAT);
//        Button startMAT = (Button) findViewById(R.id.startMAT);
//        Button resetMAT = (Button) findViewById(R.id.resetMAT);
        Button stopMAT = (Button) findViewById(R.id.stopMAT);

        createMAT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateMATClick(v);
            }
        });
//        startMAT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onStartMATClick(v);
//            }
//        });

        // audio demo layout
        LinearLayout audioExample = (LinearLayout) findViewById(R.id.audio_demo);
        TextView audTitle = (TextView) audioExample.findViewById(R.id.AudioDemoTitle);
//        audTitle.setText("Three-track example");
        audTitle.setText("Two-track example");
        RadioGroup audioOptions = (RadioGroup) audioExample.findViewById(R.id.AudioOptions);

        addOptionRadios("airportannouncement_none_hdphns", "airportannouncement_army95_hdphns", null/*"anml_7084_stereo_0db", "anml_7084_stereo_20db", "anml_7084_stereo_40db"*/, null, audioOptions);

        audioOptions.setOnCheckedChangeListener(
                new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        int index;
                        Log.d(tag, "checkedId: " + checkedId);

                        RadioButton rb = (RadioButton) findViewById(checkedId);
                        String buttonText = rb.getText().toString();

                        index = Arrays.asList(filenames).indexOf(buttonText);
                        Log.d(tag, "index: " + index);

                        mat.switchTo(index);
                    }
                }
        );

        ToggleButton playStop = (ToggleButton) findViewById(R.id.PlayStop);
        playStop.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    if (isChecked) {
                        mat.startPlaying();

//                        Button startMAT = (Button) findViewById(R.id.startMAT);
//                        startMAT.setEnabled(false);
//                        Button resetMAT = (Button) findViewById(R.id.resetMAT);
//                        resetMAT.setEnabled(true);
                        Button stopMAT = (Button) findViewById(R.id.stopMAT);
                        stopMAT.setEnabled(true);
                    } else {
                        mat.reset();

//                        Button startMAT = (Button) findViewById(R.id.startMAT);
//                        startMAT.setEnabled(true);
//                        Button resetMAT = (Button) findViewById(R.id.resetMAT);
//                        resetMAT.setEnabled(false);
                        Button stopMAT = (Button) findViewById(R.id.stopMAT);
                        stopMAT.setEnabled(false);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

//        resetMAT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onResetMATClick(v);
//            }
//        });

        stopMAT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStopMATClick(v);
            }
        });
    }

    private void addOptionRadios(String option1, String option2, String option3, String option4, RadioGroup rG) {
        // layout with options
        LinearLayout optionStack = new LinearLayout(this);
        optionStack.setOrientation(LinearLayout.VERTICAL);

        if (option1 != null) {
            RadioButton newRButton = new RadioButton(this);
            newRButton.setText(option1);
            rG.addView(newRButton);
            // addTextView(activity, optionStack, option1);
        }
        if (option2 != null) {
            RadioButton newRButton = new RadioButton(this);
            newRButton.setText(option2);
            rG.addView(newRButton);
            // addTextView(activity, optionStack, option2);
        }
        if (option3 != null) {
            RadioButton newRButton = new RadioButton(this);
            newRButton.setText(option3);
            rG.addView(newRButton);
            // addTextView(activity, optionStack, option3);
        }
        if (option4 != null) {
            RadioButton newRButton = new RadioButton(this);
            newRButton.setText(option4);
            rG.addView(newRButton);
            // addTextView(activity, optionStack, option4);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onCreateMATClick(View v) {
        mat = new MultipleAudioTrack(this, filenames);

        Button createMAT = (Button) findViewById(R.id.createMAT);
        createMAT.setEnabled(false);
//        Button startMAT = (Button) findViewById(R.id.startMAT);
//        startMAT.setEnabled(true);
    }

    private void onStartMATClick(View v) {
        try {
            mat.startPlaying();

            ToggleButton playStop = (ToggleButton) findViewById(R.id.PlayStop);
            playStop.setChecked(true);

//            Button startMAT = (Button) findViewById(R.id.startMAT);
//            startMAT.setEnabled(false);
//            Button resetMAT = (Button) findViewById(R.id.resetMAT);
//            resetMAT.setEnabled(true);
            Button stopMAT = (Button) findViewById(R.id.stopMAT);
            stopMAT.setEnabled(true);
        } catch (IOException e) {
            Log.e(tag, "error starting", e);
            e.printStackTrace();
        }
    }

    private void onResetMATClick(View v) {
        try {
            mat.reset();

            ToggleButton playStop = (ToggleButton) findViewById(R.id.PlayStop);
            playStop.setChecked(false);

//            Button startMAT = (Button) findViewById(R.id.startMAT);
//            startMAT.setEnabled(true);
//            Button resetMAT = (Button) findViewById(R.id.resetMAT);
//            resetMAT.setEnabled(false);
            Button stopMAT = (Button) findViewById(R.id.stopMAT);
            stopMAT.setEnabled(false);
        } catch (InterruptedException e) {
            Log.e(tag, "error stopping", e);
            e.printStackTrace();
        }
    }
    private void onStopMATClick(View v) {
        try {
            mat.stopPlaying();

//            Button startMAT = (Button) findViewById(R.id.startMAT);
//            startMAT.setEnabled(false);
//            Button resetMAT = (Button) findViewById(R.id.resetMAT);
//            resetMAT.setEnabled(false);
            Button stopMAT = (Button) findViewById(R.id.stopMAT);
            stopMAT.setEnabled(false);
        } catch (InterruptedException e) {
            Log.e(tag, "error stopping", e);
            e.printStackTrace();
        }
    }
}
